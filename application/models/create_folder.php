<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create_Folder extends CI_Model {
	
	function createFolder($dateNow){
		$newDate = str_replace('-', '', $dateNow);
		$path = 'assets/img/news/'.$newDate;

		if(!file_exists('./assets/img/news/'.$newDate)){
		   mkdir('./assets/img/news/'.$newDate, 0777, TRUE);
		}

		return $path;
	}

	function createFolderPhotoNews($dateNow){
		$newDate = str_replace('-', '', $dateNow);
		$path = 'assets/img/photo_news/'.$newDate;

		if(!file_exists('./assets/img/photo_news/'.$newDate)){
		   mkdir('./assets/img/photo_news/'.$newDate, 0777, TRUE);
		}

		return $path;
	}

	function createFolderVideoNews($dateNow){
		$newDate = str_replace('-', '', $dateNow);
		$path = 'assets/video/video_news/'.$newDate;

		if(!file_exists('./assets/video/video_news/'.$newDate)){
		   mkdir('./assets/video/video_news/'.$newDate, 0777, TRUE);
		}

		return $path;
	}

	function createFolderDoc($dateNow){
		$newDate = str_replace('-', '', $dateNow);
		$path = 'assets/document/'.$newDate;

		if(!file_exists('./assets/document/'.$newDate)){
		   mkdir('./assets/document/'.$newDate, 0777, TRUE);
		}

		return $path;
	}

	
	
}

?>