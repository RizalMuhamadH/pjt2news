<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class Post extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }

    public function get_post_limit($start, $limit){
        $this->db->select('p.*, u.*')
            ->from("post p")
            ->join('users u', 'p.user_id = u.user_id')
            ->limit($start, $limit)
            ->order_by('p.post_id', 'DESC');

        $query = $this->db->get()->result();
        if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

    public function add_new_post($caption, $content, $cat, $date, $user){
        
        $uid = $this->check_user($user);

        $insert = array('post_id' => '', 
        'post_caption' => $caption, 
        'post_content' => $content, 
        'post_cat_id' => $cat, 
        'post_date' => $date, 
        'user_id' => $uid);

        $result = $this->db->insert('post', $insert);

        if($result){
           return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
        
        $result->free_result();

    }

    public function update_post($caption, $id, $user){
        $uid = $this->check_user($user);

        $this->db->set('post_caption', $caption);

        $this->db->where('post_id', $id);
        $this->db->where('user_id', $uid);

        $result = $this->db->update('post');

        if($result){
            return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
		}else{
		    return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
        
        $result->free_result();

    }
    
    public function delete_post($id, $user){
        $uid = $this->check_user($user);

        $this->db->where('post_id', $id);
        $this->db->where('user_id', $uid);

        $result = $this->db->delete('post');

        if($result){
            return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
		}else{
		    return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
        
        $result->free_result();
        
    }

    public function check_user($id_user){
         $this->db->select('user_id')
        ->from('users')
        ->where('nik', $id_user);

        $get_data = $this->db->get()->result();

        $row = $get_data[0];

        return $row->user_id;
    }

}