<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class News extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }

    public function get_all_news(){
        $q = "SELECT n.news_id, n.news_title, n.news_summary, n.thumb_path, n.category_id, n.news_date,
            cat.category_name,
            (SELECT COUNT(*) FROM liked l WHERE l.news_id=n.news_id) AS liked,
            (SELECT COUNT(*) FROM comment c WHERE c.news_id=n.news_id) AS comment,
            (SELECT COUNT(view_hits) FROM view v WHERE v.news_id=n.news_id) AS comment
            FROM news AS n 
            INNER JOIN news_cat AS cat USING(category_id)
            ORDER BY n.news_id DESC";
		$query = $this->db->query($q)->result();
		if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }
    
    public function get_news_by_category($cat){
       $q = "SELECT n.news_id, n.news_title, n.news_summary, n.thumb_path, n.category_id, n.news_date,
            cat.category_name,
            (SELECT COUNT(*) FROM liked l WHERE l.news_id=n.news_id) AS liked,
            (SELECT COUNT(*) FROM comment c WHERE c.news_id=n.news_id) AS comment,
            (SELECT COUNT(view_hits) FROM view v WHERE v.news_id=n.news_id) AS comment
            FROM news AS n 
            INNER JOIN news_cat AS cat USING(category_id)
            WHERE category_id = ".$cat."
            ORDER BY n.news_id DESC";
		$query = $this->db->query($q)->result();
		if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

    public function get_news_limit($limit=5){
        $q = "SELECT n.news_id, n.news_title, n.news_summary, n.thumb_path, n.category_id, n.news_date,
            cat.category_name,
            (SELECT COUNT(*) FROM liked l WHERE l.news_id=n.news_id) AS liked,
            (SELECT COUNT(*) FROM comment c WHERE c.news_id=n.news_id) AS comment,
            (SELECT COUNT(view_hits) FROM view v WHERE v.news_id=n.news_id) AS comment
            FROM news AS n 
            INNER JOIN news_cat AS cat USING(category_id)
            ORDER BY n.news_id DESC
            LIMIT 0, {$limit}";
		$query = $this->db->query($q)->result();
		if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

    public function get_detail_news($id){
        $this->db->where('news_id', $id);

        $query = $this->db->get('news')->result();

        if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

}
    
