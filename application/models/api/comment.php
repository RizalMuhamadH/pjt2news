<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class Comment extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }

    public function get_comment_by_news($id){
        $this->db->select('c.*, u.*')
                ->from("comment c")
                ->join('users u', 'c.user_id = u.user_id')
                ->where('c.news_id', $id)
                ->order_by('c.comment_id', 'DESC');

        $query = $this->db->get()->result();
        if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

    public function save_comment_news_by_user($id_news, $id_user, $content, $date){
        $this->db->select('user_id')
        ->from('users')
        ->where('nik', $id_user);;

        $get_data = $this->db->get()->result();

        $row = $get_data[0];

        $uid = $row->user_id;


        $data = array('comment_id' => '', 
            'comment_content' => $content,
            'comment_date' => $date,
            'news_id' => $id_news, 
            'user_id' => $uid);

        $result = $this->db->insert('comment', $data);

        if ($result != null) {
            return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
        }else{
            return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
    }

    public function remove_comment_news_by_user($id_news, $id_user){
        $this->db->select('user_id')
        ->from('users')
        ->where('nik', $id_user);

        $get_data = $this->db->get()->result();

        $row = $get_data[0];

        $uid = $row->user_id;

        $this->db->where('news_id', $id_news);
        $this->db->where('user_id', $uid);

        $query = $this->db->delete('comment');

        if ($query != null) {
            return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
        }else{
            return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
    }

}