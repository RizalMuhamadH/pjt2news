<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class User extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }

    public function get_detail_user($nik){
        $this->db->where('user_nik', $nik);
        $query = $this->db->get('users')->result();
        return $query;
    }
}