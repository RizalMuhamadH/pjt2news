<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class Liked_Post extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }

    public function get_like_post($id){
        $this->db->where('post_id', $id);

        $query = $this->db->get('liked_post')->result();

        if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

    public function get_count_like_post($id){
        $this->db->where('post_id', $id);

        $query = $this->db->get('liked_post')->num_rows();

        if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

    public function get_like_post_by_user($id, $id_user){
        
        $uid = $this->check_user($user);

        $q = "SELECT * FROM liked_post WHERE post_id = ".$id." AND user_id = ".$uid;

        $query = $this->db->query($q)->result();

        if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
    }

    public function save_liked_post_by_user($id, $id_user){
        
        $uid = $this->check_user($user);

        $data = array('like_post_id' => '', 
            'post_id' => $id, 
            'user_id' => $uid);

        $result = $this->db->insert('liked_post', $data);

        if ($result != null) {
            return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
        }else{
            return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
    }

    public function remove_liked_post_by_user($id, $id_user){
        
        $uid = $this->check_user($user);

        $this->db->where('post_id', $id);
        $this->db->where('user_id', $uid);

        $query = $this->db->delete('liked_post');

        if ($query != null) {
            return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
        }else{
            return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
    }

    public function check_user($id_user){
         $this->db->select('user_id')
        ->from('users')
        ->where('nik', $id_user);

        $get_data = $this->db->get()->result();

        $row = $get_data[0];

        return $row->user_id;
    }
    
}