<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class Auth extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }

    public function check_auth($nik){
        $this->db->where('user_nik', $nik);
        $query = $this->db->get('user')->row();
        return $query;
    }
}