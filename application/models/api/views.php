<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class Views extends CI_Model {

	public function __construct()
	{
		parent::__construct();
    }

    public function add_view_news($id_news, $id_user){

       $uid = $this->check_user($id_user);

       $q = "SELECT * FROM view WHERE news_id = ".$id_news." AND user_id =".$uid;

       $query = $this->db->query($q);

       if ($query == null){
           $insert = array('view_id' => '', 
           'view_hits' => 1, 
           'news_id' => $id_news, 
           'user_id' => $uid);

           $result = $this->db->insert('view', $insert);
       } else{
           $q = "UPDATE view SET view_hits = view_hits + 1 WHERE news_id = ".$id_news." AND user_id = ".$uid;
           
           $result = $this->db->query($q);
       }

       if($result){
           return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
        
        $result->free_result();
    }


    public function check_user($id_user){
         $this->db->select('user_id')
        ->from('users')
        ->where('nik', $id_user);

        $get_data = $this->db->get()->result();

        $row = $get_data[0];

        return $row->user_id;
    }
}