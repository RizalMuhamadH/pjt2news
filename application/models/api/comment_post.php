<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** [!] Legacy API Model for Mobile ***/
class Comment_Post extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_comment_post_by_post($id){
        $this->db->select('cp.*, u.*')
                ->from("comment_post cp")
                ->join('users u', 'cp.user_id = u.user_id')
                ->where('cp.news_id', $id)
                ->order_by('cp.comment_post_id', 'DESC');

        $query = $this->db->get()->result();
        if ($query != null) {
			return $response = array('status' => 'success', 'kode' => 200, 'data' => $query);
		}else{
			return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'empty');
        }
    }

    public function save_comment_post_by_user($id_post, $id_user, $content, $date){
    
        $uid = $this->check_user($user);


        $data = array('comment_post_id' => '', 
            'comment_post_content' => $content,
            'comment_post_date' => $date,
            'post_id' => $id_post, 
            'user_id' => $uid);

        $result = $this->db->insert('comment_post', $data);

        if ($result != null) {
            return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
        }else{
            return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
    }

    public function remove_comment_post_by_user($id_post, $id_user){
		
		$uid = $this->check_user($user);

        $this->db->where('post_id', $id_post);
        $this->db->where('user_id', $uid);

        $query = $this->db->delete('comment_post');

        if ($query != null) {
            return $response = array('status' => 'success', 'kode' => 200, 'data' => 'true');
        }else{
            return $response = array('status' => 'failed', 'kode' => 502, 'data' => 'false');
        }
	}
	
	public function check_user($id_user){
         $this->db->select('user_id')
        ->from('users')
        ->where('nik', $id_user);

        $get_data = $this->db->get()->result();

        $row = $get_data[0];

        return $row->user_id;
	}
	
}