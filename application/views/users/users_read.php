<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Users Read</h2>
        <table class="table">
	    <tr><td>User Nik</td><td><?php echo $user_nik; ?></td></tr>
	    <tr><td>User Name</td><td><?php echo $user_name; ?></td></tr>
	    <tr><td>User Email</td><td><?php echo $user_email; ?></td></tr>
	    <tr><td>User Birthday</td><td><?php echo $user_birthday; ?></td></tr>
	    <tr><td>Gender</td><td><?php echo $gender; ?></td></tr>
	    <tr><td>Path Profile</td><td><?php echo $path_profile; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('users') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>