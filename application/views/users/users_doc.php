<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Users List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>User Nik</th>
		<th>User Name</th>
		<th>User Email</th>
		<th>User Birthday</th>
		<th>Gender</th>
		<th>Path Profile</th>
		
            </tr><?php
            foreach ($users_data as $users)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $users->user_nik ?></td>
		      <td><?php echo $users->user_name ?></td>
		      <td><?php echo $users->user_email ?></td>
		      <td><?php echo $users->user_birthday ?></td>
		      <td><?php echo $users->gender ?></td>
		      <td><?php echo $users->path_profile ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>