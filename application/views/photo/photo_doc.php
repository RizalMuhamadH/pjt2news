<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Photo List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Photo Path</th>
		<th>Photo Caption</th>
		<th>Photo Date</th>
		
            </tr><?php
            foreach ($photo_data as $photo)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $photo->photo_path ?></td>
		      <td><?php echo $photo->photo_caption ?></td>
		      <td><?php echo $photo->photo_date ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>