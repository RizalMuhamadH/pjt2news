<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Photo Read</h2>
        <table class="table">
	    <tr><td>Photo Path</td><td><?php echo $photo_path; ?></td></tr>
	    <tr><td>Photo Caption</td><td><?php echo $photo_caption; ?></td></tr>
	    <tr><td>Photo Date</td><td><?php echo $photo_date; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('photo') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>