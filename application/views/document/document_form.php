<!doctype html>
<html>
    <head>
    </head>
    <body>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Document
                <small><?php echo $button; ?></small>
                </h1>
                <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>document">Document</a></li>
                <li class="active"><?php echo $button; ?></li>
                <!--
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
                -->
                </ol>
            </section>

            <!-- Main content -->
	        <section class="content">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Document <?php echo $button; ?></h3>
                    </div> <!-- box-header -->
                    <div class="box-body">
                        <form action="<?php echo $action; ?>" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="doc_path" value="<?php echo $document_path; ?>">
                        <div class="form-group">
                            <label for="varchar">Document Name <?php echo form_error('document_name') ?></label>
                            <input type="text" class="form-control" name="document_name" id="document_name" placeholder="Document Name" value="<?php echo $document_name; ?>" />
                        </div>
                        <div class="form-group">
                            <label for="document_path">Document Path <?php echo form_error('document_path') ?></label>
                            <input type="file" class="form-control" name="document_path" id="document_path" placeholder="Document Path"/>
                        </div>
                        <input type="hidden" name="document_id" value="<?php echo $document_id; ?>" /> 
                        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                        <a href="<?php echo site_url('document') ?>" class="btn btn-default">Cancel</a>
                    </form>
                    </div> <!-- box-body -->
                </div> <!-- box-info -->
            </section><!-- content -->
                

        </div> <!-- wrapper -->
    </body>
</html>