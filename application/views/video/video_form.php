<!doctype html>
<html>
    <head>
    </head>
    <body>
        <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    Video
                    <small><?php echo $button; ?></small>
                    </h1>
                    <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a href="<?php echo base_url(); ?>video">Video</a></li>
                    <li class="active"><?php echo $button; ?></li>
                    <!--
                    <li><a href="#">Layout</a></li>
                    <li class="active">Top Navigation</li>
                    -->
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Form Video <?php echo $button; ?></h3>
                        </div> <!-- box-header -->
                        <div class="box-body">
                            <form action="<?php echo $action; ?>" enctype="multipart/form-data" method="post">

                            <input type="hidden" name="video_tmp" value="<?php echo $video_path; ?>">
                            <div class="form-group">
                                    <label for="title">Video Title <?php echo form_error('video_title') ?></label>
                                    <input type="text" class="form-control" name="video_title" id="video_title" placeholder="Video Title" value="<?php echo $video_title; ?>" />
                                </div>
                                <div class="form-group custom-file">
                                    <label class="custom-file-label" for="video_path">Video Path <?php echo form_error('video_path') ?></label>
                                    <input type="file" class="form-control custom-file-input" rows="3" name="video_path" id="video_path" placeholder="Video Path"/>
                                </div>
                                
                                <input type="hidden" name="video_id" value="<?php echo $video_id; ?>" /> 
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i><?php echo $button ?></button> 
                                <a href="<?php echo site_url('video') ?>" class="btn btn-default">Cancel</a>
                            </form>
                        </div> <!-- box-body -->
                </div> <!-- box-info -->
            </section><!-- content -->
                

        </div> <!-- wrapper -->
    </body>
</html>