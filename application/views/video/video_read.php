<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Video Read</h2>
        <table class="table">
        <tr><td>Video Title</td><td><?php echo $video_title; ?></td></tr>
	    <tr><td>Video Path</td><td><video width="500" controls><source src="<?php echo base_url().$video_path; ?>"></video></td></tr>
	    <tr><td>Video Date</td><td><?php echo $video_date; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('video') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>