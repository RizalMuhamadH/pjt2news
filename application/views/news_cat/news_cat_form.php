<!doctype html>
<html>
    <head>
    </head>
    <body>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Category News
                <small><?php echo $button; ?></small>
                </h1>
                <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>news_cat">News</a></li>
                <li class="active"><?php echo $button; ?></li>
                <!--
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
                -->
                </ol>
            </section>

            <!-- Main content -->
	        <section class="content">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Category News <?php echo $button; ?></h3>
                    </div> <!-- box-header -->
                

                    <div class="box-body">
                        <!-- <h2 style="margin-top:0px">News_cat <?php echo $button ?></h2> -->
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label for="varchar">Category Name <?php echo form_error('category_name') ?></label>
                                <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Category Name" value="<?php echo $category_name; ?>" />
                            </div>
                            <input type="hidden" name="category_id" value="<?php echo $category_id; ?>" /> 
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button> 
                            <a href="<?php echo site_url('news_cat') ?>" class="btn btn-default">Cancel</a>
                        </form>
                    </div> <!-- box-body -->
                </div> <!-- box-info -->
            </section><!-- content -->
                

        </div> <!-- wrapper -->

    </body>
</html>