<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">News Read</h2>
        <table class="table">
	    <tr><td>News Title</td><td><?php echo $news_title; ?></td></tr>
	    <tr><td>News Content</td><td><?php echo $news_content; ?></td></tr>
	    <tr><td>News Summary</td><td><?php echo $news_summary; ?></td></tr>
	    <tr><td>Thumb Path</td><td><?php echo $thumb_path; ?></td></tr>
	    <tr><td>Category Id</td><td><?php echo $category_id; ?></td></tr>
	    <tr><td>News Date</td><td><?php echo $news_date; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('news') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>