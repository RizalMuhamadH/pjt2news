<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>News List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>News Title</th>
		<th>News Content</th>
		<th>News Summary</th>
		<th>Thumb Path</th>
		<th>Category Id</th>
		<th>News Date</th>
		
            </tr><?php
            foreach ($news_data as $news)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $news->news_title ?></td>
		      <td><?php echo $news->news_content ?></td>
		      <td><?php echo $news->news_summary ?></td>
		      <td><?php echo $news->thumb_path ?></td>
		      <td><?php echo $news->category_id ?></td>
		      <td><?php echo $news->news_date ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>