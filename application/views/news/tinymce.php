<script>
	tinymce.init({
		selector: "textarea",
		theme: "modern",
		skin:"lightgray",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality emoticons template paste textcolor importcss colorpicker textpattern"
		],
		external_plugins: {
			//"moxiemanager": "/moxiemanager-php/plugin.js"
		},
		content_css: '//www.tinymce.com/css/codepen.min.css',
		add_unload_trigger: false,
		image_dimensions: false,
		image_description: false,
		image_advtab: true,
		
		toolbar: "insertfile undo redo | formatselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons table | fontsizeselect",

		images_upload_base_path: '/assets/img',
		images_upload_credentials: true,
		image_title: true,
		automatic_uploads: true,
		images_reuse_filename: true,
		file_picker_types: 'image',
		
		images_upload_handler: function (blobInfo, success, failure) {
		    var xhr, formData;
		    xhr = new XMLHttpRequest();
		    xhr.withCredentials = false;
		    xhr.open('POST', 'http://wowbekasi.ayomagz.com/home/uploadImage');
		    xhr.onload = function() {
		      var json;

		      if (xhr.status != 200) {
		        failure('HTTP Error: ' + xhr.status);
		        return;
		      }
		      json = JSON.parse(xhr.responseText);

		      if (!json || typeof json.location != 'string') {
		        failure('Invalid JSON: ' + xhr.responseText);
		        return;
		      }
		      success(json.location);
		    };
		    formData = new FormData();
		    formData.append('file', blobInfo.blob(), blobInfo.filename());
		    xhr.send(formData);
		},
		file_picker_callback: function(cb, value, meta) {
		    var input = document.createElement('input');
		    input.setAttribute('type', 'file');
		    input.setAttribute('accept', 'image/*');
		    
		    // Note: In modern browsers input[type="file"] is functional without 
		    // even adding it to the DOM, but that might not be the case in some older
		    // or quirky browsers like IE, so you might want to add it to the DOM
		    // just in case, and visually hide it. And do not forget do remove it
		    // once you do not need it anymore.

		    input.onchange = function() {
		      var file = this.files[0];
		      
		      var reader = new FileReader();
		      reader.onload = function () {
		        // Note: Now we need to register the blob in TinyMCEs image blob
		        // registry. In the next release this part hopefully won't be
		        // necessary, as we are looking to handle it internally.
		        // var id = 'blobid' + (new Date()).getTime();
		        var id = file.name;
		        var res = id.replace(" ", "_");
		        var firstWord = res.substr(0, res.indexOf("."));
		        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
		        var base64 = reader.result.split(',')[1];
		        var blobInfo = blobCache.create(firstWord, file, base64);
		        blobCache.add(blobInfo);

		        // call the callback and populate the Title field with the file name
		        cb(blobInfo.blobUri(), { title: file.name });
		      };
		      reader.readAsDataURL(file);
		    };
		    
		    input.click();
	  	}, 

		style_formats: [
			{title: 'Bold text', format: 'h1'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		],
		// style_formats: [
		//     { title: 'Bold text', inline: 'strong' },
		//     { title: 'Red text', inline: 'span', styles: { color: '#ff0000' } },
		//     { title: 'Red header', block: 'h1', styles: { color: '#ff0000' } },
		//     { title: 'Badge', inline: 'span', styles: { display: 'inline-block', border: '1px solid #2276d2', 'border-radius': '5px', padding: '2px 5px', margin: '0 2px', color: '#2276d2' } },
		//     { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
		//   ],
	  formats: {
	    // alignleft: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'left' },
	    // aligncenter: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'center' },
	    // alignright: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'right' },
	    // alignfull: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'full' },
	    // bold: { inline: 'b' },
	    // italic: { inline: 'i'},
	    // underline: { inline: 'u'},
	    // strikethrough: { inline: 'del' },
	    // customformat: { inline: 'span', styles: { color: '#00ff00', fontSize: '20px' }, attributes: { title: 'My custom format' }, classes: 'example1' },
		// },
			removeformat: [
			// {selector: 'b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand: true, deep : true},
			{selector: 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
			{selector: '*', attributes : ['style', 'class'], split : false, expand : false, deep : true}
			]

		},

		block_formats: 'Paragraph=p;Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Preformatted=pre',

		fontsize_formats: '1pt 2pt 3pt 4pt 5pt 6pt 7pt 8pt 10pt 12pt 14pt 18pt 24pt 36pt',

		template_preview_replace_values : {
			username : "Preview user name"
		},

		link_class_list: [
			{title: 'Example 1', value: 'example1'},
			{title: 'Example 2', value: 'example2'}
		],

		// image_class_list: [
		// 	{title: 'Example 1', value: 'example1'},
		// 	{title: 'Example 2', value: 'example2'}
		// ],

		templates: [
			{title: 'Some title 1', description: 'Some desc 1', content: '<strong class="red">My content: {$username}</strong>'},
			{title: 'Some title 2', description: 'Some desc 2', url: 'development.html'}
		],

		setup: function(ed) {
			/*ed.on(
				'Init PreInit PostRender PreProcess PostProcess BeforeExecCommand ExecCommand Activate Deactivate ' +
				'NodeChange SetAttrib Load Save BeforeSetContent SetContent BeforeGetContent GetContent Remove Show Hide' +
				'Change Undo Redo AddUndo BeforeAddUndo', function(e) {
				console.log(e.type, e);
			});*/
		},

		spellchecker_callback: function(method, data, success) {
			if (method == "spellcheck") {
				var words = data.match(this.getWordCharPattern());
				var suggestions = {};

				for (var i = 0; i < words.length; i++) {
					suggestions[words[i]] = ["First", "second"];
				}

				success({words: suggestions, dictionary: true});
			}

			if (method == "addToDictionary") {
				success();
			}
		}
	});
</script>