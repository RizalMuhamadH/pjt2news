<!doctype html>
<html>
    <head>

    <script src="<?php echo base_url(); ?>assets/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/tinymce/tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/tinymce/plugins/table/plugin.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/tinymce/plugins/paste/plugin.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/tinymce/plugins/spellchecker/plugin.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/AdminLTE-2.4.5/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
		$(function () {
			// Replace the <textarea id="editor1"> with a CKEditor
					  // instance, using default configuration.
			// CKEDITOR.replace('editor1')
			// bootstrap WYSIHTML5 - text editor
			$('.textarea1').wysihtml5();

			$('#news_content').val(tinyMCE.get('news_content').getContent());
		})
    </script>
    
        <!-- <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/> -->
    </head>
    <body>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <?php require_once('tinymce.php'); ?>
                <h1>
                News
                <small><?php echo $button; ?></small>
                </h1>
                <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="<?php echo base_url(); ?>news">News</a></li>
                <li class="active"><?php echo $button; ?></li>
                <!--
                <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li>
                -->
                </ol>
            </section>

            <!-- Main content -->
	        <section class="content">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form News <?php echo $button; ?></h3>
                    </div> <!-- box-header -->
                

                    <div class="box-body">

                        <!-- <?php require_once('tinymce.php'); ?> -->
                        <!-- <h2 style="margin-top:0px">News <?php echo $button ?></h2> -->
                        <form action="<?php echo $action; ?>" enctype="multipart/form-data" method="post">

                            <input type="hidden" name="news_thumb" value="<?php echo $thumb_path; ?>">

                            <div class="form-group">
                                <label for="varchar">Title <?php echo form_error('news_title') ?></label>
                                <input type="text" class="form-control" name="news_title" id="news_title" placeholder="Title" value="<?php echo $news_title; ?>" />
                            </div>
                            
                            <div class="form-group">
                                <label for="news_summary">Summary <?php echo form_error('news_summary') ?></label>
                                <input class="form-control" rows="3" name="news_summary" id="news_summary" placeholder="Summary" value="<?php echo $news_summary; ?>" />
                            </div>

                            <div class="form-group custom-file">
                                <label class="custom-file-label" for="varchar">Thumb<?php echo form_error('thumb_path') ?></label>
                                <input type="file" class="form-control custom-file-input" name="thumb_path" id="thumb_path" placeholder="Thumb" value="<?php echo $thumb_path; ?>" />
                            </div>

                            <div class="form-group">
                                <label for="int">Category <?php echo form_error('category_id') ?></label>
                                <!-- <input type="text" class="form-control" name="category_id" id="category_id" placeholder="Category" value="<?php echo $category_id; ?>" /> -->
                                <select class="form-control" name="category_id">
                                    <?php
                                        foreach ($cat as $lihat):
                                    ?>
                                        <option value="<?php echo $lihat->category_id;?>" <?php if($category_id==$lihat->category_id) echo 'selected="selected"'; ?> ><?php echo $lihat->category_name;?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="news_content">Content <?php echo form_error('news_content') ?></label>
                                <textarea class="form-control textarea1" rows="3" name="news_content" id="news_content" placeholder="Content" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $news_content; ?></textarea>
                            </div>

                            

                            <!-- <div class="form-group">
                                <label for="date">News Date <?php echo form_error('news_date') ?></label>
                                <input type="text" class="form-control" name="news_date" id="news_date" placeholder="News Date" value="<?php echo $news_date; ?>" />
                            </div> -->

                            <input type="hidden" name="news_id" value="<?php echo $news_id; ?>" /> 
                            
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> <?php echo $button ?></button> 
                            
                            <a href="<?php echo site_url('news') ?>" class="btn btn-default">Cancel</a>
                            
                        </form>

                    </div> <!-- box-body -->
                </div> <!-- box-info -->
            </section><!-- content -->
                

        </div> <!-- wrapper -->
        
    
    
    
    </body>

    
</html>




