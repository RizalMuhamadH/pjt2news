<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News_cat extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('News_cat_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function getAll(){
       $this->News_cat_model->get_all();
    }

    public function index()
    {
        $data['page'] = "news_cat/news_cat_list";

        $this->load->view('dashboard/home', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->News_cat_model->json();
    }

    public function read($id) 
    {
        $row = $this->News_cat_model->get_by_id($id);
        if ($row) {
            $data = array(
		'category_id' => $row->category_id,
		'category_name' => $row->category_name,
	    );
            $this->load->view('news_cat/news_cat_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('news_cat'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('news_cat/create_action'),
            'category_id' => set_value('category_id'),
            'category_name' => set_value('category_name'),
            'page' => 'news_cat/news_cat_form'
	);
        $this->load->view('dashboard/home', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'category_name' => $this->input->post('category_name',TRUE),
	    );

            $this->News_cat_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('news_cat'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->News_cat_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('news_cat/update_action'),
                'category_id' => set_value('category_id', $row->category_id),
                'category_name' => set_value('category_name', $row->category_name),
                'page' => 'news_cat/news_cat_form',
	    );
            $this->load->view('dashboard/home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('news_cat'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('category_id', TRUE));
        } else {
            $data = array(
		'category_name' => $this->input->post('category_name',TRUE),
	    );

            $this->News_cat_model->update($this->input->post('category_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('news_cat'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->News_cat_model->get_by_id($id);

        if ($row) {
            $this->News_cat_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('news_cat'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('news_cat'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('category_name', 'category name', 'trim|required');

	$this->form_validation->set_rules('category_id', 'category_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "news_cat.xls";
        $judul = "news_cat";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Name");

	foreach ($this->News_cat_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_name);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=news_cat.doc");

        $data = array(
            'news_cat_data' => $this->News_cat_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('news_cat/news_cat_doc',$data);
    }

}

/* End of file News_cat.php */
/* Location: ./application/controllers/News_cat.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-27 10:00:20 */
/* http://harviacode.com */