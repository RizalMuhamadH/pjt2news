<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Document extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Document_model');
        $this->load->library('form_validation');       
        $this->load->model('create_folder');         
	    $this->load->library('datatables');
    }

    public function index()
    {
        $data['page'] = 'document/document_list';
        $this->load->view('dashboard/home', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Document_model->json();
    }

    public function read($id) 
    {
        $row = $this->Document_model->get_by_id($id);
        if ($row) {
            $data = array(
		'document_id' => $row->document_id,
		'document_name' => $row->document_name,
		'document_path' => $row->document_path,
	    );
            $this->load->view('document/document_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('document'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('document/create_action'),
	    'document_id' => set_value('document_id'),
	    'document_name' => set_value('document_name'),
        'document_path' => set_value('document_path'),
        'page' => 'document/document_form'
	);
        $this->load->view('dashboard/home', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->create();
        // } else {
            $date_now = date('y-m-d');

            $path = '';
            $folder_path = $this->create_folder->createFolderDoc($date_now);

            // setting konfigurasi upload
            $config['upload_path'] = './assets/document/'.str_replace('-', '', $date_now);
            $config['allowed_types'] = 'pdf';
            // $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('document_path')) {
                $error = $this->upload->display_errors();
                // menampilkan pesan error
                // print_r($error);
                $path = $error;
            } else {
                $result = $this->upload->data();
                $path = $folder_path.'/'.$result['file_name'];
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            }

            $data = array(
		'document_name' => $this->input->post('document_name',TRUE),
		'document_path' => $path,
	    );

            $this->Document_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('document'));
        // }
    }
    
    public function update($id) 
    {
        $row = $this->Document_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('document/update_action'),
		'document_id' => set_value('document_id', $row->document_id),
		'document_name' => set_value('document_name', $row->document_name),
		'document_path' => set_value('document_path', $row->document_path),
	    'page' => 'document/document_form'
	    );
        $this->load->view('dashboard/home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('document'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($this->input->post('document_id', TRUE));
        // } else {
            $date_now = date('y-m-d');

            $path = $this->input->post('doc_path');
            $folder_path = $this->create_folder->createFolderDoc($date_now);

            // setting konfigurasi upload
            $config['upload_path'] = './assets/document/'.str_replace('-', '', $date_now);
            $config['allowed_types'] = 'pdf';
            // $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('document_path')) {
                $error = $this->upload->display_errors();
                // menampilkan pesan error
                // print_r($error);
                $path = $error;
            } else {
                $result = $this->upload->data();
                $path = $folder_path.'/'.$result['file_name'];
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            }

            $data = array(
		'document_name' => $this->input->post('document_name',TRUE),
		'document_path' => $this->input->post('document_path',TRUE),
	    );

            $this->Document_model->update($this->input->post('document_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('document'));
        // }
    }
    
    public function delete($id) 
    {
        $row = $this->Document_model->get_by_id($id);

        if ($row) {
            $this->Document_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('document'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('document'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('document_name', 'document name', 'trim|required');
	// $this->form_validation->set_rules('document_path', 'document path', 'trim|required');

	$this->form_validation->set_rules('document_id', 'document_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "document.xls";
        $judul = "document";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Document Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Document Path");

	foreach ($this->Document_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->document_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->document_path);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=document.doc");

        $data = array(
            'document_data' => $this->Document_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('document/document_doc',$data);
    }

}

/* End of file Document.php */
/* Location: ./application/controllers/Document.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-08-01 12:22:53 */
/* http://harviacode.com */