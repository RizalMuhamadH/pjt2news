<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('News_model');
		$this->load->model('News_cat_model');
		$this->load->model('Photo_model');
		$this->load->model('Video_model');
		$this->load->model('Users_model');
		
    }

	public function index(){
		
		$data['page'] = "dashboard/index";
		
		$data['count_user'] = $this->Users_model->total_rows();
		$data['count_article'] = $this->News_model->total_rows();
		$data['count_news_cat'] = $this->News_cat_model->total_rows();
		$data['count_foto'] = $this->Photo_model->total_rows();
		$data['count_video'] = $this->Video_model->total_rows();
	
		$this->load->view('dashboard/home', $data);
	}
}
