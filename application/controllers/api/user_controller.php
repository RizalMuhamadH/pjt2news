<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  require APPPATH . '../vendor/autoload.php';
  use \Firebase\JWT\JWT;

class User_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('api/user');
    $this->load->model('api/key');
    $this->load->model('api/auth');
  }

  public function detailUser(){
      
      $nik = $this->input->post('nik');
      $authentication = $this->input->get_request_header('Authorization');

      $query = $this->user->get_detail_user($nik);

      if ($query != null) {
            $encode = JWT::encode($query, $this->key->get_key());
		    echo json_encode(array('status' => 'success', 'kode' => 200, 'data' => $encode));
		}else{
			echo json_encode(array('status' => 'failed', 'kode' => 502, 'data' => 'empty'));
        }
  }

  public function getAuth($authentication){
      try{
          $decode = JWT::decode($authentication, $this->key->get_key(),array('HS256'));
          if ($this->auth->check_auth($decode->username)) {
              # code...
          } else {
              # code...
          }
          
      } catch(Exception $e){

      }
  }
}

