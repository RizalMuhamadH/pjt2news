<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('views');
  }

  public function addViews(){
      $id = $this->input->post('id_news');
      $nik = $this->input->post('nik');

      $respone = $this->views->add_view_news($id, $nik);

      echo json_encode($respone);
  }
}