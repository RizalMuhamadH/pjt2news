<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('post');
  }

  public function getPostLimit(){
      $start = $this->input->post('start');
      $limit = $this->input->post('limit');

      $respone = $this->post->get_post_limit($start, $limit);

      echo json_encode($respone);
  }

  public function addNewPost(){
    $caption = $_REQUEST["caption"];
    $dateNow = date('d-m-y');
    $cat = $_REQUEST["category"];
    $id_user = $_REQUEST["id_user"];

    if ($cat == "1") {
      # code...
    } else {

      $folder = $this->folderPath($id_user, $cat);

      $filename = str_replace(" ", "_",basename($_FILES["file"]["name"]));
      
      $destiny = '.'.$folder.'/'.$filename;

      $file_path = $folder.'/'.$filename;


      if ($cat == "2") {

        if (move_uploaded_file($_FILES["file"]["tmp_name"],$destiny)) {      
          $response = $this->post->add_new_post($caption, $file_path, $cat, $dateNow, $id_user);
          echo json_encode($response);
        } else{
          $response = array('status' => 'failed', 'kode' => 502);
          echo json_encode($response);
        }

      } else if ($cat == "3") {
        
        if (move_uploaded_file($_FILES["file"]["tmp_name"],$destiny)) {      
          $response = $this->post->add_new_post($caption, $file_path, $cat, $dateNow, $id_user);
          echo json_encode($response);
        } else{
          $response = array('status' => 'failed', 'kode' => 502);
          echo json_encode($response);
        }

      }

    }
    
  }

  public function updatePost(){

    $caption = $this->input->post('caption');
    $id_user = $this->input->post('id_user');
    $id_post = $this->input->post('id_post');

    $response = $this->post->update_post($caption, $id_post, $id_user);
    echo json_encode($response);
  }

  public function deletePost(){

    $id_user = $this->input->post('id_user');
    $id_post = $this->input->post('id_post');

    $response = $this->post->delete_post($id_post, $id_user);
    echo json_encode($response);
    
  }

  public function folderPath($id_user, $cat){
    if ($cat == "2") {
      if(!file_exists('./assets/img/'.$id_user)){
        mkdir('./assets/img/'.$id_user, 0777, TRUE);
        return '/assets/img/'.$id_user;
      } else {
        return '/assets/img/'.$id_user;
      }
    } elseif ($cat == "3") {
      if(!file_exists('./assets/video/'.$id_user)){
        mkdir('./assets/video/'.$id_user, 0777, TRUE);
        return '/assets/video/'.$id_user;
      } else {
        return '/assets/video/'.$id_user;
      }
    }
  }
}