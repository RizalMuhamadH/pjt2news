<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('comment');
  }

  public function getCommentNews(){
      $id = $this->input->post('id');

      $respone = $this->comment->get_comment_by_news($id);

      echo json_encode($respone);
  }

  public function saveCommentNews(){
      $id = $this->input->post('id_news');
      $nik = $this->input->post('nik');
      $content = $this->input->post('content');
      $date = date('Y-m-d');

      $respone = $this->comment->save_comment_news_by_user($id, $nik, $content, $date);

      echo json_encode($respone);
  }

  public function removeCommentNews(){
      $id = $this->input->post('id_news');
      $nik = $this->input->post('nik');

      $respone = $this->comment->remove_comment_news_by_user($id, $nik);

      echo json_encode($respone);
  }
}