<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  require APPPATH . '../vendor/autoload.php';
  use \Firebase\JWT\JWT;

class News_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('news');
  }

  public function getAllNews(){
      $respone = $this->news->get_all_news();
      echo json_encode($respone);
  }

  public function getNewsByCategory(){
    $cat = $this->input->post('cat');

    $respone = $this->news->get_news_by_category($cat);
    echo json_encode($respone);
  }

  public function getNewsLimit(){
    $limit = $this->input->post('limit');

    $respone = $this->news->get_news_limit($limit);

    echo json_encode($respone);
  }
}