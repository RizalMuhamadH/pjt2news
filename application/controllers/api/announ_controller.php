<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announ_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('announ');
  }
  
  public function getAllAnnoun(){
      $respone = $this->announ->get_all_announ();

      echo json_encode($respone);
  }

  public function getDetailAnnoun(){
      $id = $this->input->post('id');

      $respone = $this->announ->get_detail_announ($id);

      echo json_encode($respone);
  }
}