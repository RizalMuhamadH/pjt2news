<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment_Post_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('comment_post');
  }

  public function getCommentPost(){
      $id = $this->input->post('id');

      $respone = $this->comment_post->get_comment_post_by_post($id);

      echo json_encode($respone);
  }

  public function saveCommentPost(){
      $id = $this->input->post('id_post');
      $nik = $this->input->post('nik');
      $content = $this->input->post('content');
      $date = date('Y-m-d');

      $respone = $this->comment_post->save_comment_post_by_user($id, $nik, $content, $date);

      echo json_encode($respone);
  }

  public function removeCommentPost(){
      $id = $this->input->post('id_post');
      $nik = $this->input->post('nik');

      $respone = $this->comment_post->remove_comment_post_by_user($id, $nik);

      echo json_encode($respone);
  }
}