 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Liked_Controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('liked');
  }

  public function getLikeNews(){
      $id = $this->input->post('id_news');

      $respone = $this->liked->get_like_news($id);

      echo json_encode($respone);
  }

  public function getCountLikeNews(){
      $id = $this->input->post('id_news');

      $respone = $this->liked->get_count_like_news($id);

      echo json_encode($respone);
  }

  public function getLikeNewsByUser(){
      $id = $this->input->post('id_news');
      $nik = $this->input->post('nik');

      $respone = $this->liked->get_like_news_by_user($id, $nik);

      echo json_encode($respone);
  }

  public function saveLikeNews(){
      $id = $this->input->post('id_news');
      $nik = $this->input->post('nik');

      $respone = $this->liked->save_liked_news_by_user($id, $nik);

      echo json_encode($respone);
  }

  public function removeLikeNews(){
      $id = $this->input->post('id_news');
      $nik = $this->input->post('nik');

      $respone = $this->liked->remove_liked_news_by_user($id, $nik);

      echo json_encode($respone);
  }
}