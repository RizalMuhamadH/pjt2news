<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Photo extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Photo_model');
        $this->load->library('form_validation');
        $this->load->model('create_folder');        
	    $this->load->library('datatables');
    }

    public function index()
    {
        $data['page'] = 'photo/photo_list';
        $this->load->view('dashboard/home', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Photo_model->json();
    }

    public function read($id) 
    {
        $row = $this->Photo_model->get_by_id($id);
        if ($row) {
            $data = array(
		'photo_id' => $row->photo_id,
		'photo_path' => $row->photo_path,
		'photo_caption' => $row->photo_caption,
		'photo_date' => $row->photo_date,
	    );
            $this->load->view('photo/photo_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('photo'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('photo/create_action'),
	    'photo_id' => set_value('photo_id'),
	    'photo_path' => set_value('photo_path'),
	    'photo_caption' => set_value('photo_caption'),
        'photo_date' => set_value('photo_date'),
        'page' => 'photo/photo_form',
	);
        $this->load->view('dashboard/home', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->create();
        // } else {
            $date_now = date('y-m-d');

            $thumb_path = '';
            $folder_path = $this->create_folder->createFolderPhotoNews($date_now);

            // setting konfigurasi upload
            $config['upload_path'] = './assets/img/photo_news/'.str_replace('-', '', $date_now);
            $config['allowed_types'] = 'jpg|png|jpeg';
            // $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('photo_path')) {
                $error = $this->upload->display_errors();
                // menampilkan pesan error
                // print_r($error);
                $thumb_path = $error;
            } else {
                $result = $this->upload->data();
                $thumb_path = $folder_path.'/'.$result['file_name'];
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            }

            $data = array(
                'photo_path' => $thumb_path,
                'photo_caption' => $this->input->post('photo_caption',TRUE),
                'photo_date' => $date_now,
            );

            $this->Photo_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('photo'));
        // }
    }
    
    public function update($id) 
    {
        $row = $this->Photo_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('photo/update_action'),
		'photo_id' => set_value('photo_id', $row->photo_id),
		'photo_path' => set_value('photo_path', $row->photo_path),
		'photo_caption' => set_value('photo_caption', $row->photo_caption),
        'photo_date' => set_value('photo_date', $row->photo_date),
        'page' => 'photo/photo_form',
	    );
            $this->load->view('dashboard/home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('photo'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($this->input->post('photo_id', TRUE));
        // } else {

            $date_now = date('y-m-d');

            $thumb_path = $this->input->post('photo_thumb',TRUE);
            $folder_path = $this->create_folder->createFolderPhotoNews($date_now);

            // setting konfigurasi upload
            $config['upload_path'] = './assets/img/photo_news/'.str_replace('-', '', $date_now);
            $config['allowed_types'] = 'jpg|png|jpeg';
            // $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('photo_path')) {
                $error = $this->upload->display_errors();
                // menampilkan pesan error
                // print_r($error);
            } else {
                $result = $this->upload->data();
                $thumb_path = $folder_path.'/'.$result['file_name'];
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            }

            $data = array(
                'photo_path' => $thumb_path,
                'photo_caption' => $this->input->post('photo_caption',TRUE),
                // 'photo_date' => $this->input->post('photo_date',TRUE),
            );

            $this->Photo_model->update($this->input->post('photo_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('photo'));
        // }
    }
    
    public function delete($id) 
    {
        $row = $this->Photo_model->get_by_id($id);

        if ($row) {
            $this->Photo_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('photo'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('photo'));
        }
    }

    public function _rules() 
    {
	// $this->form_validation->set_rules('photo_path', 'photo path', 'trim|required');
	$this->form_validation->set_rules('photo_caption', 'photo caption', 'trim|required');
	$this->form_validation->set_rules('photo_date', 'photo date', 'trim|required');

	$this->form_validation->set_rules('photo_id', 'photo_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "photo.xls";
        $judul = "photo";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Photo Path");
	xlsWriteLabel($tablehead, $kolomhead++, "Photo Caption");
	xlsWriteLabel($tablehead, $kolomhead++, "Photo Date");

	foreach ($this->Photo_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->photo_path);
	    xlsWriteLabel($tablebody, $kolombody++, $data->photo_caption);
	    xlsWriteLabel($tablebody, $kolombody++, $data->photo_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=photo.doc");

        $data = array(
            'photo_data' => $this->Photo_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('photo/photo_doc',$data);
    }

}

/* End of file Photo.php */
/* Location: ./application/controllers/Photo.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-31 04:53:57 */
/* http://harviacode.com */