<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('News_model');
        $this->load->model('News_cat_model');
        $this->load->model('create_folder');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->load->helper(array('url','form'));
    }

    public function index(){

        $data['page'] = "news/news_list";

        $this->load->view('dashboard/home', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->News_model->json();
    }

    public function read($id) 
    {
        $row = $this->News_model->get_by_id($id);
        if ($row) {
            $data = array(
                'news_id' => $row->news_id,
                'news_title' => $row->news_title,
                'news_content' => $row->news_content,
                'news_summary' => $row->news_summary,
                'thumb_path' => $row->thumb_path,
                'category_id' => $row->category_id,
                'news_date' => $row->news_date,
	    );
            $this->load->view('news/news_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('news'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('news/create_action'),
            'news_id' => set_value('news_id'),
            'news_title' => set_value('news_title'),
            'news_content' => set_value('news_content'),
            'news_summary' => set_value('news_summary'),
            'thumb_path' => set_value('thumb_path'),
            'category_id' => set_value('category_id'),
            'news_date' => set_value('news_date'),
            'page' => 'news/news_form',
            'cat' => $this->News_cat_model->get_all(),
    );
    

        $this->load->view('dashboard/home', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->create();
        // } else {

            $content = $this->input->post('news_content',TRUE);

            $check_content_1 = str_replace("../assets", base_url()."assets", $content);
		    $check_content_2 = str_replace("../../assets", base_url()."assets", $check_content_1);

            $date_now = date('y-m-d');

            $thumb_path = '';
            $folder_path = $this->create_folder->createFolder($date_now);

            // setting konfigurasi upload
            $config['upload_path'] = './assets/img/news/'.str_replace('-', '', $date_now);
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('thumb_path')) {
                $error = $this->upload->display_errors();
                // menampilkan pesan error
                // print_r($error);
            } else {
                $result = $this->upload->data();
                $thumb_path = $folder_path.'/'.$result['file_name'];
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            }

            $data = array(
                'news_title' => $this->input->post('news_title',TRUE),
                'news_content' => $check_content_2,
                'news_summary' => $this->input->post('news_summary',TRUE),
                'thumb_path' => $thumb_path,
                'category_id' => $this->input->post('category_id',TRUE),
                'news_date' => $date_now,
	    );

            $this->News_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('news'));
        // }
    }
    
    public function update($id) 
    {
        $row = $this->News_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('news/update_action'),
                'news_id' => set_value('news_id', $row->news_id),
                'news_title' => set_value('news_title', $row->news_title),
                'news_content' => set_value('news_content', $row->news_content),
                'news_summary' => set_value('news_summary', $row->news_summary),
                'thumb_path' => set_value('thumb_path', $row->thumb_path),
                'category_id' => set_value('category_id', $row->category_id),
                'news_date' => set_value('news_date', $row->news_date),
                'page' => 'news/news_form',
                'cat' => $this->News_cat_model->get_all(),
	    );
            $this->load->view('dashboard/home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('news'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($this->input->post('news_id', TRUE));
        // } else {

            $content = $this->input->post('news_content',TRUE);

            $check_content_1 = str_replace("../assets", base_url()."assets", $content);
		    $check_content_2 = str_replace("../../assets", base_url()."assets", $check_content_1);

            $date_now = date('y-m-d');

            $thumb_path = $this->input->post('news_thumb',TRUE);

            $folder_path = $this->create_folder->createFolder($date_now);

            // setting konfigurasi upload
            $config['upload_path'] = './assets/img/news/'.str_replace('-', '', $date_now);
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('thumb_path')) {
                $error = $this->upload->display_errors();
                // menampilkan pesan error
                // print_r($error);
            } else {
                $result = $this->upload->data();
                $thumb_path = $folder_path.'/'.$result['file_name'];
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            }

            $data = array(
                'news_title' => $this->input->post('news_title',TRUE),
                'news_content' => $check_content_2,
                'news_summary' => $this->input->post('news_summary',TRUE),
                'thumb_path' => $thumb_path,
                'category_id' => $this->input->post('category_id',TRUE),
                // 'news_date' => $this->input->post('news_date',TRUE),
	    );

            $this->News_model->update($this->input->post('news_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('news'));
        // }
    }
    
    public function delete($id) 
    {
        $row = $this->News_model->get_by_id($id);

        if ($row) {
            $this->News_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('news'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('news'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('news_title', 'news title', 'trim|required');
	$this->form_validation->set_rules('news_content', 'news content', 'trim|required');
	$this->form_validation->set_rules('news_summary', 'news summary', 'trim|required');
	// $this->form_validation->set_rules('thumb_path', 'thumb path', 'callback_file_check');
	$this->form_validation->set_rules('category_id', 'category id', 'trim|required');
	$this->form_validation->set_rules('news_date', 'news date', 'trim|required');

	$this->form_validation->set_rules('news_id', 'news_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "news.xls";
        $judul = "news";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "News Title");
        xlsWriteLabel($tablehead, $kolomhead++, "News Content");
        xlsWriteLabel($tablehead, $kolomhead++, "News Summary");
        xlsWriteLabel($tablehead, $kolomhead++, "Thumb Path");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Id");
        xlsWriteLabel($tablehead, $kolomhead++, "News Date");

	foreach ($this->News_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->news_title);
            xlsWriteLabel($tablebody, $kolombody++, $data->news_content);
            xlsWriteLabel($tablebody, $kolombody++, $data->news_summary);
            xlsWriteLabel($tablebody, $kolombody++, $data->thumb_path);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->news_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=news.doc");

        $data = array(
            'news_data' => $this->News_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('news/news_doc',$data);
    }

}

/* End of file News.php */
/* Location: ./application/controllers/News.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-26 08:39:22 */
/* http://harviacode.com */