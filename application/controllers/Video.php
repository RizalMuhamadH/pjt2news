<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Video extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Video_model');
        $this->load->library('form_validation');
        $this->load->model('create_folder');                
	    $this->load->library('datatables');
    }

    public function index()
    {
        $data['page'] = 'video/video_list';
        $this->load->view('dashboard/home', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Video_model->json();
    }

    public function read($id) 
    {
        $row = $this->Video_model->get_by_id($id);
        if ($row) {
            $data = array(
        'video_id' => $row->video_id,
        'video_title' => $row->video_title,
		'video_path' => $row->video_path,
		'video_date' => $row->video_date,
	    );
            $this->load->view('video/video_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('video'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('video/create_action'),
        'video_id' => set_value('video_id'),
        'video_title' => set_value('video_title'),
	    'video_path' => set_value('video_path'),
        'video_date' => set_value('video_date'),
        'page' => 'video/video_form'
	);
        $this->load->view('dashboard/home', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->create();
        // } else {

            $date_now = date('y-m-d');

            $thumb_path = '';
            $folder_path = $this->create_folder->createFolderVideoNews($date_now);

            // setting konfigurasi upload
            $config['upload_path'] = './assets/video/video_news/'.str_replace('-', '', $date_now);
            $config['allowed_types'] = 'avi|mov|mp4|mpg|mpeg|mkv|flv';
            $config['remove_spaces'] = TRUE;
            // $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('video_path')) {
                $error = $this->upload->display_errors();
                // menampilkan pesan error
                // print_r($error);
                $thumb_path = $error;
            } else {
                $result = $this->upload->data();
                $thumb_path = $folder_path.'/'.$result['file_name'];
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            }

            $data = array(
                'video_title' => $this->input->post('video_title'),
                'video_path' => $thumb_path,
                'video_date' => $date_now,
	    );

            $this->Video_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('video'));
        // }
    }
    
    public function update($id) 
    {
        $row = $this->Video_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('video/update_action'),
        'video_id' => set_value('video_id', $row->video_id),
        'video_title' => set_value('video_title', $row->video_title),
		'video_path' => set_value('video_path', $row->video_path),
        'video_date' => set_value('video_date', $row->video_date),
        'page' => 'video/video_form'
	    );
            $this->load->view('dashboard/home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('video'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($this->input->post('video_id', TRUE));
        // } else {

            $date_now = date('y-m-d');

            $thumb_path = $this->input->post('video_tmp');
            $folder_path = $this->create_folder->createFolderPhotoNews($date_now);

            // setting konfigurasi upload
            $config['upload_path'] = './assets/video/video_news/'.str_replace('-', '', $date_now);
            $config['allowed_types'] = 'avi|mov|mp4|mpg|mpeg|mkv|flv';
            // $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('video_path')) {
                $error = $this->upload->display_errors();
                // menampilkan pesan error
                // print_r($error);
                // $thumb_path = $error;
            } else {
                $result = $this->upload->data();
                $thumb_path = $folder_path.'/'.$result['file_name'];
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            }

            $data = array(
		'video_path' => $thumb_path,
		'video_title' => $this->input->post('video_title',TRUE),
	    );

            $this->Video_model->update($this->input->post('video_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('video'));
        // }
    }
    
    public function delete($id) 
    {
        $row = $this->Video_model->get_by_id($id);

        if ($row) {
            $this->Video_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('video'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('video'));
        }
    }

    public function _rules() 
    {
    // $this->form_validation->set_rules('video_path', 'video path', 'trim|required');
    $this->form_validation->set_rules('video_title', 'video title', 'trim|required');
	$this->form_validation->set_rules('video_date', 'video date', 'trim|required');

	$this->form_validation->set_rules('video_id', 'video_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "video.xls";
        $judul = "video";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Video Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Video Path");
	xlsWriteLabel($tablehead, $kolomhead++, "Video Date");

	foreach ($this->Video_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->video_path);
	    xlsWriteLabel($tablebody, $kolombody++, $data->video_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=video.doc");

        $data = array(
            'video_data' => $this->Video_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('video/video_doc',$data);
    }

}

/* End of file Video.php */
/* Location: ./application/controllers/Video.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-31 06:00:34 */
/* http://harviacode.com */