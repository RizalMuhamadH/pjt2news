<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $data['page'] = 'users/users_list';
        $this->load->view('dashboard/home', $data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Users_model->json();
    }

    public function read($id) 
    {
        $row = $this->Users_model->get_by_id($id);
        if ($row) {
            $data = array(
		'user_id' => $row->user_id,
		'user_nik' => $row->user_nik,
		'user_name' => $row->user_name,
		'user_email' => $row->user_email,
		'user_birthday' => $row->user_birthday,
		'gender' => $row->gender,
		'path_profile' => $row->path_profile,
	    );
            $this->load->view('users/users_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users/create_action'),
	    'user_id' => set_value('user_id'),
	    'user_nik' => set_value('user_nik'),
	    'user_name' => set_value('user_name'),
	    'user_email' => set_value('user_email'),
	    'user_birthday' => set_value('user_birthday'),
	    'gender' => set_value('gender'),
        'path_profile' => set_value('path_profile'),
        'page' => 'users/users_form',
	);
        $this->load->view('dashboard/home', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->create();
        // } else {
            $data = array(
		'user_nik' => $this->input->post('user_nik',TRUE),
		'user_name' => $this->input->post('user_name',TRUE),
		'user_email' => $this->input->post('user_email',TRUE),
		'user_birthday' => $this->input->post('user_birthday',TRUE),
		'gender' => $this->input->post('gender',TRUE),
		'path_profile' => '',
	    );

            $this->Users_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users'));
        // }
    }
    
    public function update($id) 
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users/update_action'),
		'user_id' => set_value('user_id', $row->user_id),
		'user_nik' => set_value('user_nik', $row->user_nik),
		'user_name' => set_value('user_name', $row->user_name),
		'user_email' => set_value('user_email', $row->user_email),
		'user_birthday' => set_value('user_birthday', $row->user_birthday),
		'gender' => set_value('gender', $row->gender),
		'path_profile' => set_value('path_profile', $row->path_profile),
        'page' => 'users/users_form',
	    );
            $this->load->view('dashboard/home', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($this->input->post('user_id', TRUE));
        // } else {
            $data = array(
		'user_nik' => $this->input->post('user_nik',TRUE),
		'user_name' => $this->input->post('user_name',TRUE),
		'user_email' => $this->input->post('user_email',TRUE),
		'user_birthday' => $this->input->post('user_birthday',TRUE),
		'gender' => $this->input->post('gender',TRUE),
		// 'path_profile' => $this->input->post('path_profile',TRUE),
	    );

            $this->Users_model->update($this->input->post('user_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users'));
        // }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $this->Users_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user_nik', 'user nik', 'trim|required');
	$this->form_validation->set_rules('user_name', 'user name', 'trim|required');
	$this->form_validation->set_rules('user_email', 'user email', 'trim|required');
	$this->form_validation->set_rules('user_birthday', 'user birthday', 'trim|required');
	$this->form_validation->set_rules('gender', 'gender', 'trim|required');
	$this->form_validation->set_rules('path_profile', 'path profile', 'trim|required');

	$this->form_validation->set_rules('user_id', 'user_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "users.xls";
        $judul = "users";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "User Nik");
	xlsWriteLabel($tablehead, $kolomhead++, "User Name");
	xlsWriteLabel($tablehead, $kolomhead++, "User Email");
	xlsWriteLabel($tablehead, $kolomhead++, "User Birthday");
	xlsWriteLabel($tablehead, $kolomhead++, "Gender");
	xlsWriteLabel($tablehead, $kolomhead++, "Path Profile");

	foreach ($this->Users_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->user_nik);
	    xlsWriteLabel($tablebody, $kolombody++, $data->user_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->user_email);
	    xlsWriteLabel($tablebody, $kolombody++, $data->user_birthday);
	    xlsWriteLabel($tablebody, $kolombody++, $data->gender);
	    xlsWriteLabel($tablebody, $kolombody++, $data->path_profile);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=users.doc");

        $data = array(
            'users_data' => $this->Users_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('users/users_doc',$data);
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-27 11:28:03 */
/* http://harviacode.com */